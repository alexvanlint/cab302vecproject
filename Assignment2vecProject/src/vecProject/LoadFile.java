package vecProject;

import java.io.BufferedReader;
import java.io.FileReader;

public class LoadFile {

    /**
     * @
     */

    private String[] modFile;   // File split into array
    private String loadedFile;  // File initial loaded as single string

    StringBuilder stringBuilder = new StringBuilder();

    /**
     *
     * @param filePath  directory of file to load + name of file
     * @throws Exception  IO exception will be thrown when directory / file doesn't exist
     * Both of the above cases are handled by the GUI (to be implimented)
     */

    public LoadFile(String filePath) throws Exception {

        String loadedLine;

        BufferedReader br = new BufferedReader(new FileReader(filePath));

        while((loadedLine = br.readLine()) != null) {
            stringBuilder.append(loadedLine);
            stringBuilder.append("\n");
        }

        this.loadedFile = stringBuilder.toString();
    }

    /**
     * ReturnLoadedFile()
     * @return  returns a single string of the entire file once loaded
     * Method was implimented primarily for testing purposes and allows for an unmodded file if necessary
     */
    public String ReturnLoadedFile() {
//        System.out.println(loadedFile); // println being used to ensure entire file is initially read to "buffer"
        return loadedFile;
    }

    /**
     * ReturnModdedFile()
     * @return  returns a string array splitting the loaded file by lines
     * Primary method that will be referenced as each line of the array can be referenced to execute commands instead of filtering through single string
     */
    public String[] ReturnModdedFile() {

        modFile = loadedFile.split("\n");

        // dirty println test to ensure entire file is loaded correctly
//        for(int i = 0; i < modFile.length; i++) {
//            System.out.println(modFile[i]);
//        }

        return modFile;
    }

}
