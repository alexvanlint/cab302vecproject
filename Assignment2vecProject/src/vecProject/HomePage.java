package vecProject;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomePage extends JFrame implements ActionListener, Runnable{

    /**
     * Spagetti coded GUI
     * current iteration is lacking features so full functionality / code cleanup has yet to occur
     * variables currently fully named to provide extra detail as comments are not present and changes to overal design and functionality need to be made.
     */

    private static HomePage vecGUI;

    private String[] openProjectData;

    public HomePage() {
        initComponents();
        vecGUI = this;
    }

    private void initComponents() {
        JButton plotBTN, lineBTN, rectBTN, ellipseBTN, ployBTN, fillBTN, penClrBTN, saveBTN, loadBTN;

        plotBTN = new JButton("Plot");
        lineBTN = new JButton("Line");
        rectBTN = new JButton("Rectangle");
        ellipseBTN = new JButton("Ellipse");
        ployBTN = new JButton("Poly");
        fillBTN = new JButton("Fill");
        penClrBTN = new JButton("Pen Colour");

        saveBTN = new JButton("Save");
        loadBTN = new JButton("Load");

        JPanel toolButtons = new JPanel();
        toolButtons.setLayout(new GridLayout(7,1));

        JPanel menuButtons = new JPanel();
        menuButtons.setLayout(new GridLayout(1,2));

        JPanel canvas = new JPanel();

        toolButtons.add(plotBTN);
        toolButtons.add(lineBTN);
        toolButtons.add(rectBTN);
        toolButtons.add(ellipseBTN);
        toolButtons.add(ployBTN);
        toolButtons.add(fillBTN);
        toolButtons.add(penClrBTN);

        menuButtons.add(saveBTN);
        saveBTN.addActionListener(this);
        menuButtons.add(loadBTN);
        loadBTN.addActionListener(this);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(toolButtons, BorderLayout.WEST);
        mainPanel.add(menuButtons, BorderLayout.NORTH);
        mainPanel.add(canvas,BorderLayout.EAST);

        getContentPane().add(mainPanel);
    }

    public void run() {
        openGUI();
    }

    private static void openGUI() {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        JFrame.setDefaultLookAndFeelDecorated(true);

        int scaledWidth = (int)(screenSize.getWidth()*0.90);
        int scaledHeight = (int)(screenSize.getHeight()*0.90);
        int xPoint = (int)(screenSize.getWidth() - scaledWidth) / 2;
        int yPoint = (int)(screenSize.getHeight() - scaledHeight) / 2;

        vecGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vecGUI.setResizable(false);
        vecGUI.setPreferredSize(new Dimension(scaledWidth,scaledHeight));
        vecGUI.setLocation(new Point(xPoint, yPoint));
        vecGUI.setTitle("CAB302 Vec Editor");
        vecGUI.pack();
        vecGUI.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new HomePage());
    }

    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();

        switch(action) {
            case "Save":
                try {
                    saveFile();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case "Load":
                try {
                    loadFile();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
        }

    }

    private void saveFile() throws Exception {
        JFileChooser fileIO = new JFileChooser(System.getProperty("user.dir"));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".vec Files","vec");
        fileIO.setFileFilter(filter);

        SaveFile saveFileAction;

        int returnVal = fileIO.showSaveDialog(null);

        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String fileName = fileIO.getName(fileIO.getSelectedFile());
            String directory = fileIO.getCurrentDirectory().toString();
            saveFileAction = new SaveFile(directory,fileName,openProjectData);
        }

    }

    private void loadFile() throws Exception {
        JFileChooser fileIO = new JFileChooser(System.getProperty("user.dir"));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".vec Files","vec");
        fileIO.setFileFilter(filter);

        LoadFile loadFileAction;

        int returnVal = fileIO.showOpenDialog(null);

        if(returnVal == JFileChooser.APPROVE_OPTION) {
            loadFileAction = new LoadFile(fileIO.getSelectedFile().getAbsolutePath());
            openProjectData = loadFileAction.ReturnModdedFile();
        }
    }

}