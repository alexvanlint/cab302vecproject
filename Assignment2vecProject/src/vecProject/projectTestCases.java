package vecProject;

import java.io.File;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class projectTestCases {

    LoadFile testLoadFile;
    SaveFile testSaveFile;
    ActionHandler testActionHandler;

    String filePath = new File(System.getProperty("user.dir")).getParent() + "\\Example vec files";

    String filePath1 = new File(System.getProperty("user.dir")).getParent() + "\\Example vec files\\example1.vec";
    String filePath2 = new File(System.getProperty("user.dir")).getParent() + "\\Example vec files\\example2.vec";
    String filePath3 = new File(System.getProperty("user.dir")).getParent() + "\\Example vec files\\example3.vec";

    @BeforeEach
    public void testLoadFile() {
        testLoadFile = null;
    }


    // ------------- Testing three example files load without error -------------
    @Test
    public void testFileLoaded1() throws Exception {
        testLoadFile = new LoadFile(filePath1);
        assertNotEquals(testLoadFile.ReturnLoadedFile(),"");
    }

    @Test
    public void testFileLoaded2() throws Exception {
        testLoadFile = new LoadFile(filePath2);
        assertNotEquals(testLoadFile.ReturnLoadedFile(),"");
    }

    @Test
    public void testFileLoaded3() throws Exception {
        testLoadFile = new LoadFile(filePath3);
        assertNotEquals(testLoadFile.ReturnLoadedFile(),"");
    }


    // ------------- Testing specified line("2") from file 1 loads without error -------------

    @Test
    public void testFileModified() throws Exception {
        testLoadFile = new LoadFile(filePath1);
        String[] testModdedFile = testLoadFile.ReturnModdedFile();
        assertEquals(testModdedFile[1],"LINE 1.000000 0.000000 1.000000 1.000000");
    }


    // ------------- Testing saving files -------------
    @Test
    public void testSavingFileNoExtension() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\saveFileTest.txt");
        String[] saveFileTest = testLoadFile.ReturnModdedFile();

        testSaveFile = new SaveFile(filePath, "testVecSaveNE", saveFileTest);
    }

    @Test
    public void testSavingFileWithExtension() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\saveFileTest.txt");
        String[] saveFileTest = testLoadFile.ReturnModdedFile();

        testSaveFile = new SaveFile(filePath, "testVecSaveWE.vec", saveFileTest);
    }

    @Test
    public void testSavingFileOverwrite() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\saveFileTest2.txt");
        String[] saveFileTest = testLoadFile.ReturnModdedFile();

        testSaveFile = new SaveFile(filePath, "testVecSaveWE.vec", saveFileTest);
    }

    // ------------- Testing action handler -------------
        // Tests are only to observe the system.out.println test cases in the action switch
    @Test
    public void testVecContainsLINE() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\ActionLineTest.vec");
        String[] testVecFile = testLoadFile.ReturnModdedFile();

        testActionHandler = new ActionHandler(testVecFile);
        testActionHandler.draw();

    }

    @Test
    public void testVecContainsRECTANGLE() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\ActionRectTest.vec");
        String[] testVecFile = testLoadFile.ReturnModdedFile();

        testActionHandler = new ActionHandler(testVecFile);
        testActionHandler.draw();
    }

    @Test
    public void testVecContainsELLIPSE() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\ActionEllipseTest.vec");
        String[] testVecFile = testLoadFile.ReturnModdedFile();

        testActionHandler = new ActionHandler(testVecFile);
        testActionHandler.draw();
    }

    @Test
    public void testVecContainsPLOT() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\ActionPlotTest.vec");
        String[] testVecFile = testLoadFile.ReturnModdedFile();

        testActionHandler = new ActionHandler(testVecFile);
        testActionHandler.draw();
    }

    @Test
    public void testVecContainsPOLYGON() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\ActionPolyTest.vec");
        String[] testVecFile = testLoadFile.ReturnModdedFile();

        testActionHandler = new ActionHandler(testVecFile);
        testActionHandler.draw();
    }

    @Test
    public void testVecContainsPEN() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\saveFileTest2.txt");
    }

    @Test
    public void testVecContainsFILL() throws Exception {
        testLoadFile = new LoadFile(filePath + "\\saveFileTest2.txt");
    }

}
