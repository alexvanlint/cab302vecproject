package vecProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SaveFile {

    /**
     * @param filePath  link to the directory of the file
     * @param fileName  file name entered by the vec program user (does not need to contain the file extension)
     * @param fileData  internal representation of the final vec file
     * @throws IOException  will throw an error if the directory does not exist
     */

    public SaveFile(String filePath, String fileName, String[] fileData) throws IOException {

        // Ensures that the file will save as a .vec
            // Takes into account the user added the extension in the save file name
        if(!fileName.endsWith(".vec")) {
            fileName = fileName + ".vec";
        }


        // Setting file path / buffered writer
        File saveFile = new File(filePath + "\\" + fileName);
        BufferedWriter writer = new BufferedWriter(new FileWriter(saveFile));

        // Creates or "Overwrites" file
            // Overwrite is currently deleting and recreating, need to add warning for user to ensure they wish to overwrite current file
        if (saveFile.createNewFile() == false) {
            saveFile.delete();
        }

        // Writing string array to now empty file
        for(int i = 0; i < fileData.length; i++) {
            writer.append(fileData[i]);
            writer.append("\n");
        }

        // Save and exit file
        writer.close();
    }

}
